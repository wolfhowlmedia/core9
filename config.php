<?php
//Initial dirs
define('TEMPLATE_PATH',  'templates/');
define('MODULE_PATH', 	 'modules/');
define('TRANSLATE_PATH', 'i18n/');

//DB connector (NOT needed for framework to work)
define('DB_USER', 	'root');
define('DB_PASS', 	'');
define('DB_HOST', 	'localhost');
define('DB_DATABASE', 	'');

//page title
define('PAGE_NAME',	'Page name');
define('DEFAULT_MODULE', 'index');


//Cookie URI
define('SESSION_COOKIE_URI', 'example.com');

//Page URI
define('PAGE_URI', 'http://example.com/project/');

//Full URL address
define('MAIN_PAGE_URI', 'http://example.com/project/');

//Static pages (JS, CSS, pictures...)
define('STATIC_PAGE_URI', '');


