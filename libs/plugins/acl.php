<?php
//Simple ACL class.
//Requires SQL connection.
//TODO: Add support for levels
class ACL {
	//prijava
	static public function login($username, $password) {
		$db = DBCore::get_instance();
		$sc = Session::get_instance();
		
		$query = "
			SELECT username, email, joined, can_comment, can_upload, is_confirmed, is_admin
			FROM members 
			WHERE username = ? AND password = ?";
		$out = $db->fetch($query, array($username, sha1($password)));
		
		if (is_array($out) && sizeof($out)) {
			$sc->storeSession('USER', $out, true);
			
			return 'true';
		} else {
			return 'false';
		}
	}
	
	//Logout
	static public function logout() {
		$sc = Session::get_instance();
		$sc->destroy_session();
	}
}
