<?php
//User class
class User {
	//Gather user information from session
	static public function get_user() {
		$sc = Session::getInstance();
		
		$user = $sc->loadSession('USER');
		if (!$user) {
			return array();
		} else {
			return $user;
		}
		
		return ;
	}
	
	//Does the user exist?
	static public function user_exists($username) {
		$db = DBCore::getInstance();
		$res = $db->fetch("SELECT * FROM members WHERE username=?", array($username));
		return (bool) sizeof($res);
	}
	
	//Is email already in the database?
	static public function email_exists($email) {
		$db = DBCore::getInstance();
		$res = $db->fetch("SELECT * FROM members WHERE email=?", array($email));
		return (bool) sizeof($res);
	}
	
	//Check username sanity	 (a-Z0-9-)
	static public function check_sanity($input) {
		$input = trim($input);
		if (strlen ($input) < 2 || !preg_match('#^[a-z0-9][a-z0-9\-]+[a-z0-9]$#i', $input)) {
			return false;
		} else {
			return true;
		}
	}
}
