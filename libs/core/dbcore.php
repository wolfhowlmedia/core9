<?php
//Database handling methods
class DBCore {
	private static $_database;
	private $db = null;
	
	private function __construct() {
		// private constructor restricts instantiaton to getInstance()
        try {
			$this->db = new PDO('mysql:host='. DB_HOST .';dbname='. DB_DATABASE, DB_USER, DB_PASS);
			$this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	    } catch(Exception $e) {
			die("DB connection failed");
	    }
	}
	
	protected function __clone() {
		// restricts cloning of the object
	}

	static public function get_instance() {
		if(is_null(self::$_database)) {
			self::$_database = new self();
		}
		return self::$_database;
	}

	
	public function exec($sql) {
		//execute raw database command
		$this->db->exec($sql);
	}
	
	public function fetch($sql, $binds = array(), $fetchone = false, $mode = PDO::FETCH_ASSOC) {
		//Return data from fetched row(s)
		if ($fetchone) {
			$sql .= ' LIMIT 1';
		}
		
		$sth = $this->db->prepare($sql);
		$sth->execute($binds);
		
		if ($fetchone) {
			return $sth->fetch($mode);
		} else {
			return $sth->fetchAll($mode);
		}
	}
	
	public function update($sql, $binds = array()) {
		//update rows
		//return number of affected rows
		$sth = $this->db->prepare($sql);
		$sth->execute($binds);
		
		return $sth->rowCount();
	}
	
	public function insert($sql, $binds = array()) {
		//insert rows
		//return the ID of the last inserted row
		$sth = $this->db->prepare($sql);
		$sth->execute($binds);
		
		return $this->db->lastInsertId();
	}
	
	public function delete($sql, $binds = array()) {
		//delete rows
		//return number of affected rows
		$sth = $this->db->prepare($sql);
		$sth->execute($binds);
		
		return $sth->rowCount();
	}
}