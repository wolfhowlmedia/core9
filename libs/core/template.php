<?php
//Template rendering class
class Template {
	private $out = '';
	private $data;
	public $add_data = array();
	
	//Load template
	public function __construct($file, $module_data = null) {
		$this->templatefile = $file;
		$this->module_data = $module_data;
	}
	
	//Cleanup HTML
	static public function html($input) {
		return htmlspecialchars($input, ENT_QUOTES, 'UTF-8');
	}
	
	//Cleanup URL
	static public function url($input) {
		return urlencode($input);
	}
	
	//Add non-standard data
	public function add_data($key, $value) {
		$this->add_data[$key] = $value;
	}
	
	//Render template or return 404
	public function render() {
		if (is_null($this->templatefile)) {
			if (isset($this->module_data['post'])) {
				echo $this->module_data['post'];
				die;
			}
			if (isset($this->module_data['get'])) {
				echo $this->module_data['get'];
				die;
			}
		} else if ($this->module_data['get'] == ERROR_NOT_FOUND) {
			header("Status: 404 Not Found");
			$this->data = $this->module_data;
			ob_start();
			include TEMPLATE_PATH . 'notfound.tpl.php';
			$this->out = ob_get_clean();
		} else if (file_exists(TEMPLATE_PATH . $this->templatefile)) {
			$this->data = $this->module_data;
			ob_start();
			include TEMPLATE_PATH . $this->templatefile;
			$this->out = ob_get_clean();
		} else {
			echo sprintf('File: %s doesn\'t exist', TEMPLATE_PATH . $this->templatefile);
		}
		
		return $this->out;
	}
}
