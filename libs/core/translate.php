<?php
//class for work with translations
class Translate {
	private static $translations;
	private static $storage;
	
	private function __construct() {
		// private constructor restricts instantiaton to getInstance()
	}
	
	protected function __clone() {
		// restricts cloning of the object
	}
	
	//get singleton instance
	static public function get_instance() {
		if(is_null(self::$translations)) {
			self::$translations = new self();
		}
		return self::$translations;
		
	}
	
	//load language
	public function load_language($language = '') {
		if ($language) {
			if(file_exists(TRANSLATE_PATH.'/'.$language.'.php')) {
				include_once(TRANSLATE_PATH.'/'.$language.'.php');
				self::$storage = $lc_messages;
			}
		} else if (defined('LANGUAGE')) {
			if(file_exists(TRANSLATE_PATH.'/'.LANGUAGE.'.php')) {
				include_once(TRANSLATE_PATH.'/'.LANGUAGE.'.php');
				self::$storage = $lc_messages;
			}
		}
	}
	
	//translate string
	public function tr($string) {
		if (isset(self::$storage[$string])) {
			return self::$storage[$string];
		} else {
			return $string;
		}
	}
}
