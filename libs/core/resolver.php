<?php
class Resolver {
	private static $path = '';
	
	public static function init($path) {
		//If file is only index.php, we assume it's the first page
		self::$path = $path;
	}
	
	//We get the array with which we can load modules and/or parameter
	public static function get_path_params() {
		$params = explode('/', ltrim(self::$path, '/'));
		array_shift($params);
		return $params;
	}
	
	//Render the module
	public static function render_page() {
		$tmppath = explode('/', self::$path);
		if (empty($tmppath[1])) {
			$tmppath[1] = DEFAULT_MODULE;
		}
		
		return self::load_module($tmppath[1]);
	}
	
	public static function raise_error($errortxt) {
		$template = new Template('errortemplate.tpl.php');
		$template->add_data('errortxt', $errortxt);
		print $template->render();
		die;
	}
	
	//Load module from path and process the GET / POST methods
	private static function load_module($module_name) {
		if (file_exists(MODULE_PATH."aliases.php")){
			include_once MODULE_PATH."aliases.php";
			$aliases = Aliases::get();
		} else {
			$aliases = array();
		}
		
		if (file_exists(MODULE_PATH."$module_name/$module_name.php")) {
			include_once MODULE_PATH."$module_name/$module_name.php";
			$tmp_name = 'Modules'.ucfirst($module_name);
			$render = new $tmp_name;

			$data = array();
			if (!empty($_POST)) {
				$data['post'] = $render->post();
			}
			$data['get'] = $render->get();
			$data['path'] = self::get_path_params();

			return $render->render($data);
		} else if (in_array($module_name, array_keys($aliases))) {
			if (file_exists(MODULE_PATH."$aliases[$module_name]/$aliases[$module_name].php")) {
				include_once MODULE_PATH."$aliases[$module_name]/$aliases[$module_name].php";
				$tmp_name = 'Modules'.ucfirst($aliases[$module_name]);
				$render = new $tmp_name;

				$data = array();
				if (!empty($_POST)) {
					$data['post'] = $render->post();
				}
				$data['get'] = $render->get();
				$data['path'] = self::get_path_params();

				return $render->render($data);
			} else {
				$template = new Template('notfound.tpl.php');
				return $template->render();
			}
		} else {
			$template = new Template('notfound.tpl.php');
			return $template->render();
		}
	}
}
