<?php

//Session handling method
class Session {
	private static $_session;
	
	private function __construct() {
		// private constructor restricts instantiaton to getInstance()
	}
	
	protected function __clone() {
		// restricts cloning of the object
	}

	static public function get_instance() {
		if (!session_id()) {
			session_start();
		}
		if(is_null(self::$_session)) {
			self::$_session = new self();
		}
		return self::$_session;
	}
	
	public function init() {
	
	}
	
	//Save session
	public function store_session($namespace, $data, $commit = true) {
		$_SESSION[$namespace] = $data;
		if ($commit) {
			session_commit();
		}
	}

	//Load namespace or specific key
	public function load_session($namespace, $key = null) {
		if (is_null($key)) {
			return isset($_SESSION[$namespace])? $_SESSION[$namespace] : false;
		} else {
			return isset($_SESSION[$namespace][$key])?$_SESSION[$namespace][$key] : false;
		}
	}
	
	//Remove key
	public function delete_key($namespace, $key) {
		if (isset($_SESSION[$namespace][$key])) {
			unset($_SESSION[$namespace][$key]);
		} else {
			return false;
		}
	}
	
	//Destroy session
	public function destroy_session() {
		if (session_id()) {
			@session_start();
			session_destroy();
		}
	}

	//Strictly save session
	public function commitSession() {
		session_commit();
	}
}
