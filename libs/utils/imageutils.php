<?php
define('IMAGEUTIL_RESIZE_BOTH', 1);
define('IMAGEUTIL_RESIZE_WIDTH', 2);
define('IMAGEUTIL_RESIZE_HEIGHT', 4);
define('IMAGEUTIL_RESIZE_THUMBNAIL', 8);

define('IMAGEUTIL_THUMBNAIL_WIDTH', 150);
define('IMAGEUTIL_THUMBNAIL_HEIGHT', 150);

define ('IMAGEUTIL_FONTS_DIR', 'fonts/');
define ('IMAGEUTIL_CUSTOM', 0);
define ('IMAGEUTIL_BACKGROUND', 1);

//Image manipulation class
class imageUtils {
	private $layers = array();
	private $imageLayer = null;
	private $originalImage = null;
	public $imageType = IMAGETYPE_JPEG;
	
	private $width = 0;
	private $height = 0;
	
	private $fontPosX = null;
	private $fontPosY = null;
	private $fontAngle = 0.0;
	private $layerOpacity = 100.0;
	private $setOpacity = false;
	
	//Picture creation from existing one or create new one
	public function __construct($imageCreation, $width = null, $height = null, $background = null) {
		switch($imageCreation) {
			case IMAGEUTIL_CUSTOM:
				if (isset($width, $height)) {
					$this->width = $width;
					$this->height = $height;
					$this->imageLayer = $this->createCustomImage($width, $height);
				} else {
					reportError('You need width and height for a custom image!');
				}
			break;
			case IMAGEUTIL_BACKGROUND:
				if (isset($background)) {
					$this->imageLayer = $this->createImageFromBackground($background);
				} else {
					reportError('no background image specified!');
				}
			break;
		}
	}
	
	//Create new image
	private function createCustomImage($width, $height) {
		return $this->imageLayer = imageCreateTrueColor($width, $height);
	}
	
	//Load already existing image as background
	private function createImageFromBackground( $background ) {
		$imgDim = @getImageSize($background);
		if (!$imgDim) {
			return false;
		} else {
			switch($imgDim[2]) {
				case IMAGETYPE_JPEG:
					return imageCreateFromJpeg($background);
				break;
				case IMAGETYPE_GIF:
					return imageCreateFromGif($background);
				break;
				case IMAGETYPE_PNG:
					return imageCreateFromPNG($background);
				break;
			}
		}
	}
	
	//Hex=>RGB conversion
	private function convertRgb($color) {
		$col = str_split($color, 2);
		return array(
			'red' => hexdec($col[0]),
			'green' => hexdec($col[1]),
			'blue' => hexdec($col[2])
		);
	}

	//public part
	//render picture
	public function render($type = IMAGETYPE_JPEG, $filename = null,  $quality = null) {
		if ($quality === null) {
			$quality = 100;
		}
		switch($type) {
			case IMAGETYPE_JPEG:
			default:
				if(!$filename) {
					header('content-type:image/jpeg');
				}
				imagejpeg($this->imageLayer, $filename, $quality);
			break;
			case IMAGETYPE_GIF:
				if(!$filename) {
					header('content-type:image/gif');
				}
				imagegif($this->imageLayer, $filename);
			break;
			case IMAGETYPE_PNG:
				if(!$filename) {
					header('content-type:image/png');
				}
				$quality = 90 - ($quality > 90 ? 90 : $quality);
				imagepng($this->imageLayer, $filename, (intval($quality / 10)));
			break;
			
		}
	}

	//Set background color
	public function setBgColor($color) {
		$rgb = self::convertRgb(trim($color, '#'));
		$bgColor = imagecolorallocate( $this->imageLayer, $rgb['red'], $rgb['green'], $rgb['blue']);
		imagefilledrectangle($this->imageLayer, 0, 0, $this->width, $this->height, $bgColor);
	}
	
	//Add text layer
	public function setTextLayer($text, $fontSize, $fontInfo = array(), $shadow = false, $shadowColor = '') {
		if (empty($fontInfo)) {
			return;
		} else {
			if (isset($fontInfo['color'], $fontInfo['font'])) {
				if ($this->fontPosY === null) {
					$this->fontPosY = 0;
				}
				
				if ($this->fontPosX === null) {
					$this->fontPosX = 0;
				}
				
				$rgb = self::convertRgb(trim($fontInfo['color'], '#'));
				$fontColor = imagecolorallocate( $this->imageLayer, $rgb['red'], $rgb['green'], $rgb['blue']);
				if ($shadow) {
					$sRgb = self::convertRgb(trim($shadowColor, '#'));
					$shadowColor = imagecolorallocate( $this->imageLayer, $sRgb['red'], $sRgb['green'], $sRgb['blue']);
					imagettftext($this->imageLayer, $fontSize, $this->fontAngle, $this->fontPosX + 1, $this->fontPosY + 1, $shadowColor, IMAGEUTIL_FONTS_DIR.$fontInfo['font'], $text);
				}
				
				imagettftext($this->imageLayer, $fontSize, $this->fontAngle, $this->fontPosX, $this->fontPosY, $fontColor, IMAGEUTIL_FONTS_DIR.$fontInfo['font'], $text);
				
				if ($this->setOpacity) {
					$sx = imagesx($this->imageLayer);
					$sy = imagesy($this->imageLayer);
					ImageCopyMerge ($this->imageLayer, $this->originalImage, 0, 0, 0, 0, $sx, $sy, $this->layerOpacity);
				}
				
				$this->setOpacity = false;
				$this->fontPosY = null;
				$this->fontPosX = null;
				$this->fontAngle = 0.0;
			}
		}
	}
	
	//Text position
	public function setFontPos($x, $y) {
		$this->fontPosX = $x;
		$this->fontPosY = $y;
	}
	
	//Add image layer
	public function setImageLayer($image, $posX, $posY, $opacity = 100) {
		if (!file_exists($image)) {
			reportError('The image doesn\'t exist!');
			return false;
		} else {
			$imgDim = @getImageSize($image);
			$im = false;
			if (!$imgDim) {
				return false;
			} else {
				switch($imgDim[2]) {
					case IMAGETYPE_JPEG:
						$im = imageCreateFromJpeg($image);
					break;
					case IMAGETYPE_GIF:
						$im = imageCreateFromGif($image);
					break;
					case IMAGETYPE_PNG:
						$im = imageCreateFromPNG($image);
					break;
				}
			}
			if (!$im) {
				return false;
			} else {
				imageCopyMerge($this->imageLayer, $im, $posX, $posY, 0, 0, $imgDim[0], $imgDim[1], $opacity);
				if ($this->setOpacity) {
					$sx = imagesx($this->imageLayer);
					$sy = imagesy($this->imageLayer);
					ImageCopyMerge ($this->imageLayer, $this->originalImage, 0, 0, 0, 0, $sx, $sy, $this->layerOpacity);
				}
			}
		}
	}
	
	//Layer rotation
	public function setRotation($rotation) {
		$this->fontAngle = $rotation;
	}
	
	//Layer opacity
	public function setLayerOpacity($opacity) {
		$this->layerOpacity = 100 - $opacity;
		$sx = imagesx($this->imageLayer);
		$sy = imagesy($this->imageLayer);
		$this->originalImage = ImageCreateTrueColor($sx,$sy) ;
		ImageCopyMerge ($this->originalImage, $this->imageLayer, 0, 0, 0, 0, $sx, $sy, $this->layerOpacity);
		$this->setOpacity = true;
	}
	
	//Set layer alphablending?
	public function setAlphaBlending($bool) {
		imageAlphaBlending($this->imageLayer, $bool);
	}
	
	//Set layer antialias?
	public function setAntialias($bool) {
		if(function_exists('imageAntialias')) {
			imageAntialias($this->imageLayer, $bool);
		}
	}
	
	//Save layer transparency.
	public function setSaveAlpha($bool) {
		imagesavealpha($this->imageLayer, $bool);
	}
	
	//Change image size. Default is proportional.
	public function resize($width, $height, $method = IMAGEUTIL_RESIZE_BOTH, $force = false) {
		$new_width = 0;
		$new_height = 0;
		$w = imagesx($this->imageLayer);
		$h = imagesy($this->imageLayer);
		
		if (!$force) {
			$ratio_orig = $w / $h;
			
			switch($method) {
				case IMAGEUTIL_RESIZE_BOTH:
				default:
					if ($h > $height || $w > $width) {
						if ($w > $h) {
							$new_width = $width;
							$new_height = round(($height / $w) * $h);
						} elseif($w < $h) {
							$new_height = $height;
							$new_width = round(($width / $h) * $w);
						} else {
							$new_width =  $width;
							$new_height = $height;
						}
					} else {
						$new_width = $w;
						$new_height = $h;
					}
				break;
				case IMAGEUTIL_RESIZE_WIDTH:
					$new_width = round($height * $ratio_orig);
					$new_height = $height;
				break;
				case IMAGEUTIL_RESIZE_HEIGHT:
					$new_height = round($width / $ratio_orig);
					$new_width = $width;
				break;
				case IMAGEUTIL_RESIZE_THUMBNAIL:
					if ($h > IMAGEUTIL_THUMBNAIL_HEIGHT || $w > IMAGEUTIL_THUMBNAIL_WIDTH) {
						if ($w > $h) {
							$new_width = IMAGEUTIL_THUMBNAIL_WIDTH;
							$new_height = round((IMAGEUTIL_THUMBNAIL_HEIGHT / $w) * $h);
						} elseif($w < $h) {
							$new_height = IMAGEUTIL_THUMBNAIL_HEIGHT;
							$new_width = round((IMAGEUTIL_THUMBNAIL_WIDTH / $h) * $w);
						} else {
							$new_width =  IMAGEUTIL_THUMBNAIL_WIDTH;
							$new_height = IMAGEUTIL_THUMBNAIL_HEIGHT;
						}
					} else {
						$new_width = $w;
						$new_height = $h;
					}
				break;
			}
		} else {
			switch($method) {
				case IMAGEUTIL_RESIZE_BOTH:
				default:
					$new_width = $width;
					$new_height = $height;
				break;
				case IMAGEUTIL_RESIZE_WIDTH:
					$new_width = $width;
					$new_height = $h;
				break;
				case IMAGEUTIL_RESIZE_HEIGHT:
					$new_height = $height;
					$new_width = $w;
				break;
				case IMAGEUTIL_RESIZE_THUMBNAIL:
					$new_width = IMAGEUTIL_THUMBNAIL_WIDTH;
					$new_height = IMAGEUTIL_THUMBNAIL_HEIGHT;
				break;
			}
		}
		
		$image_p = imageCreateTrueColor($new_width, $new_height);
		imageCopyResampled($image_p, $this->imageLayer, 0, 0, 0, 0, $new_width, $new_height, $w, $h);
		$this->imageLayer = $image_p;
	}
	
	//For external communication with GD library.
	public function getLayers() {
		return $this->imageLayer;
	}
	
	//Set GD layer
	public function setGDLayer($gdlayer, $position = 'left') {
		$width = imagesx($gdlayer);
		$height = imagesy($gdlayer);
		
		$imwidth = imagesx($this->imageLayer);
		$imheight = imagesy($this->imageLayer);
		
		$x = 0;
		$y = round(($imheight - $height) / 2);
		
		switch($position) {
			default:
			case 'left':
				$x = 0;
			break;
			case 'right':
				$x = $imwidth - $width;
			break;
			case 'center':
				$x = round(($imwidth - $width) / 2);
			break;
		}
		imagecopy($this->imageLayer, $gdlayer, $x, $y, 0, 0, $width, $height);
	}
	
	//Get extension from image type
	static public function getExtensionFromType($type) {
		switch($type) {
			default:
			case IMAGETYPE_JPEG:
				return '.jpg';
			break;
			case IMAGETYPE_GIF:
				return '.gif';
			break;
			case IMAGETYPE_PNG:
				return '.png';
			break;
			case IMAGETYPE_SWF:
				return '.swf';
			break;
		}
	}
}

function reportError($error) {
	echo $error;
}
?>
