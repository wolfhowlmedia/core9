<?php
//Handle EXIF data
//Requires: PHP's EXIF support
class ExifUtils {
	//Gather EXIF information (Make, model, picture orientation, etc)
	static private function getUsefulExif($exifData) {
		$usefulArray = array(
			'Make',
			'Model',
			'Orientation',
			'DateTime',
			'ExposureTime',
			'FNumber',
			'ISOSpeedRatings',
			'ExifVersion',
			'ShutterSpeedValue',
			'Flash',
			'FocalLength',
			'FlashPixVersion',
			'ColorSpace',
			'ExifImageWidth',
			'ExifImageLength',
			'InteroperabilityOffset'
		);
		
		$tmp = array();
		foreach($usefulArray as $item) {
			if (isset($exifData[$item])) {
				if ($item == 'Orientation') {
					$tmp[$item] = self::getExifRotation($exifData[$item]);
				} else {
					$tmp[$item] = $exifData[$item];
				}
			}
		}
		
		return $tmp;
	}
	
	//Convert EXIF to english strings
	static public function ExifToString($exifData) {
		$str = '';
		foreach(self::getUsefulExif($exifData) as $key=>$item) {
			$str .= Common::split_pagename($key).': '.$item."\n";
		}
		
		return $str;
	}
	
	//Convert EXIF rotation into understandable one
	static private function getExifRotation($rot) {
		$orientation = array(
			1 => 'Normal',
			2 => 'Normal (flipped)',
			3 => 'Rotated 180&deg;',
			4 => 'Rotated 180&deg;(flipped)',
			5 => 'CCW 90&deg;  (flipped)',
			6 => 'CCW 90&deg; ',
			7 => 'CW 90&deg; (flipped)',
			8 => 'CW 90&deg; '
		);
		
		return $orientation[$rot];
	}
}

/*
1  	top  	left side
2 	top 	right side
3 	bottom 	right side
4 	bottom 	left side
5 	left side 	top
6 	right side 	top
7 	right side 	bottom
8 	left side 	bottom
*/
