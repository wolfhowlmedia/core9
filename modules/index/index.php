<?php

/**
 * First page module.
 */
class ModulesIndex {
	public function get() {}
	
	public function post(){}
	
	public function render($data) {
		$data['translate'] = Translate::get_instance();
		if (empty($data['path'][0])) {
			$data['path'][0] = '';
		}

		switch(strtolower(trim($data['path'][0]))) {
			case 'si':
				$data['translate']->load_language('sl_SI');
			break;
		}
		$template  = new Template('index.tpl.php', $data);
		return $template->render();
	}
}
