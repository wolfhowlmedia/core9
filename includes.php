<?php 
//General config
require_once 'config.php';

//Common handling class (GET/POST/COOKIE, redirection,...)
require_once 'libs/core/common.php';

//Image utils
require_once 'libs/utils/imageutils.php';

//EXIF utils
require_once 'libs/utils/exifutils.php';

//Session utils
require_once 'libs/core/session.php';

//Login / logout 
//require_once 'libs/plugins/acl.php';

//User class
//require_once 'libs/plugins/user.php';

//DB handling
require_once 'libs/core/dbcore.php';

//resolver class
require_once 'libs/core/resolver.php';

//translate class
require_once 'libs/core/translate.php';

//Templating system
require_once 'libs/core/template.php';

